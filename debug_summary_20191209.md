## Summary

### Problem
Bmv2 simple switch[1](https://github.com/p4lang/behavioral-model/tree/master/targets#simple_switch)[2](https://github.com/p4lang/behavioral-model/blob/master/docs/simple_switch.md) crashes when running a P4 program that pops more elements from an array than are in the array.  This happens in hp4 in cases where the virtual device is working with a parsed representation larger than 40 bytes.  In vibrant evaluation, this line is invoked and causes the crash:

  <https://gitlab.flux.utah.edu/hp4/src/blob/master/hp4/p4src/includes/resize_pr.p4#L65>

Bmv2 is now firmly on a path of development focusing on P4-16 support and integration with the p4c compiler. This compiler is the community's suggested replacement for p4c-bmv2.  However, it does not support metadata-preserving clones, recirculates, and resubmits, a critical requirement for hp4.

### Way Ahead
It is possible that the --enable-WP4-16-stacks option, enabled by default in [bmv2 version 1.11.0](https://github.com/p4lang/behavioral-model/releases/tag/1.11.0), is causing the problem. Therefore, we will attempt:
- Using latest version of bmv2
- When setting up bmv2: `./configure --disable-WP4-16-stacks`
- Using latest version of p4c-bmv2

## Details
The invocation causing the crash in simple switch is explicitly permitted, and the result specified, in the [P4-14 language specification](https://p4.org/p4-spec/p4-14/v1.0.5/tex/p4.pdf): "Popping from an empty array(or popping more elements than are in the array) results in an empty array" (p. 42).

The behavior works in mininet but not outside of mininet (e.g., cloudlab).

I then updated simple switch to the latest version, with some evidence in release notes that any of several bug fixes might have addressed my problem.

The newer version of simple switch, when used with the evidently now obsolete p4c-bmv2 compiler, misbehaved in somewhat different ways.

I wrote a [simple P4 program to test the push primitive](https://gitlab.flux.utah.edu/dhankook/p4-projects/blob/master/projects/pop_test2/p4src/pop_test2_min.p4).  The program simply reads the first byte of the packet into the parsed representation, and inserts an extra bye, so when transmitted we should see an extra '0x00' at the front of the packet.

But simple switch / p4c-bmv2 results in a packet with one fewer byte, because the parsed representation isn't transmitted at all.  This bug was [confirmed by Nate Foster and other august figures in the P4 development community](http://lists.p4.org/pipermail/p4-dev_lists.p4.org/2019-November/004071.html).

Their recommendation: use simple switch with p4c, the newer compiler with development focused on supporting the newer language specification, [P4-16](https://p4.org/p4-spec/docs/P4-16-v1.2.0.pdf), but that also supports P4-14, by converting P4-14 programs to P4-16 first.

This recommendation comes from the idea that simple switch development has tracked the requirement to work with p4c, and has abandoned continued compatibility with p4c-bmv2.

However, [p4c doesn't properly implement metadata-preserving clone/resubmit/recirculate operations](https://github.com/p4lang/behavioral-model/blob/master/docs/simple_switch.md#restrictions-on-recirculate-resubmit-and-clone-operations).

This is a key facet of hp4 design / implementation, e.g.: <https://gitlab.flux.utah.edu/dhankook/hp4-src/blob/master/hp4/p4src/hp4.p4#L120>

In truth, the simple switch documentation suggests that [in some cases, metadata preservation *might* still work](https://github.com/p4lang/behavioral-model/blob/master/docs/simple_switch.md#hints-on-distinguishing-when-metadata-is-preserved-correctly).

I wrote a [simple program to test cloning in simple switch / p4c](https://gitlab.flux.utah.edu/dhankook/p4-projects/blob/master/projects/p4c_test/p4src/p4c_test.p4).

Testing this program confirmed that metadata preservation does not work reliably.  I was able to preserve standard metadata but not my own metadata.

Where do we go from here:

- Can't convert hp4 to P4-16, because the metadata preservation problem is fundamentally a P4-16 implementation defficiency

- Could fix PSA myself - not practical

- Could fix P4-16 implementation myself - not practical

- Revert to the last version of simple switch that reliably works with p4c-bmv2.  Last significant commit to p4c-bmv2 was September 2018; previous to that, the last significant commit to bmv2 was Spring 2018.  Change hp4 to avoid popping more elements than are valid in the array.  This might work with some additional overhead.  At a minimum, I'd need to add another table that reads a metadata field tracking the actual number of bytes extracted to the ext stack.  There are no guarantees this will not put me right back to simple switch crashing when popping "too many" elements.  If that happens, there is no hope left.
